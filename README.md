<p align="center" style="display: flex; align-items: flex-start; justify-content: center;">
<img alt="logo" title="#logo" src="src/assets/logo.png">
</p>


<h1 align="center">
     Desafio-iFood-Back - Equipe 2
</h1>

![Badge em Desenvolvimento](http://img.shields.io/static/v1?label=STATUS&message=FINALIZADO&color=GREEN&style=for-the-badge)


Tópicos
=================
<!--ts-->
   * [Sobre o projeto](#-sobre-o-projeto)
   * [Informações Gerais](#-informações-gerais)
   * [API endpoints](#-API-endpoints)
      * [Listar pedidos](#-Listar-pedidos)
      * [Cadastrar pedidos](#-Cadastrar-pedidos)
      * [Buscar pedido pelo id](#-Buscar-pedido-pelo-id)
      * [Alterar status do pedido para cancelado](#-Alterar-status-pedido-para-cancelado)
      * [Alterar status do pedido para confirmado](#-Alterar-status-pedido-para-confirmado)
      * [Buscar pedido pelo status](#-Buscar-pedido-pelo-status)
   * [Pré-requisitos](#-Pré-requisitos)
   * [Editar a aplicação ou rodar localmente](#-editar-a-aplicação-ou-rodar-localmente)
   * [Tecnologias](#-tecnologias)
   * [Time de desenvolvimento](#-time-de-desenvolvimento)

   ## 💻 Sobre o projeto

Esse projeto depende da [API de administradores][Apiadministradores] e tem como objetivo guardar informações de pedidos relacionadas administradores e usuarios.

Em resumo, o processo se dará através:
   - Administrador se logar no sistema e tem acesso a listas de produtos;
   - Cadastrar novos pedidos;
   - Buscar pedidos através do id (número individual a cada pedido);
   - Alterar status do pedido para cancelado;
   - Alterar status do pedido para confirmado;
   - Buscar pedido pelo status;

---

## Link complemetares

link para o repositório da API de usuarios no back: [API de ususarios](https://gitlab.com/dg2grupo2/back-end/api-ususarios).

link para o repositório da API de administradores no back: [API de administradores][ApiAdministradores].

link para o repositório do front: [front](https://gitlab.com/dg2grupo2/front-end/app)

link para slide explicativo e esquema do projeto: [Slide](https://docs.google.com/presentation/d/1E25l4NVkKBd4txFzbn1UeNnJnRi1W_Wq-D-KL3r9njY/edit#slide=id.g136204565f9_0_377)

---

## ⚙️ Informações Gerais
O que foi implementado durante o projeto:
1) endpoint listar pedidos;
2) endpoint cadastrar pedidos;
3) endpoint buscar pedido pelo id;
4) endpoint alterar status do pedido para cancelado;
5) endpoint alterar status do pedido para confirmado;
6) endpoint buscar pedido pelo status;

# API endpoints

## Listar pedidos.

#### `GET` (/pedidos) 

#### Retorno:
```
200 OK
{
  "content": [
    {
      "dataPedido": "2022-06-23",
      "descricao": "string",
      "id": 0,
      "idAdm": 0,
      "idUsuario": 0,
      "status": "string",
      "valorTotal": 0
    }
  ]
}
```
#### ou
```
Status: 400
```
#### ou
```
Mensagem de exceção: 500 
```

## Cadastrar pedidos.
`POST` (/pedidos) 
#### Retorno:
```
201

{
  "dataPedido": "2022-06-23",
  "descricao": "string",
  "idAdm": 0,
  "idUsuario": 0,
  "valorTotal": 0
}
```
#### ou
```
Status: 400
```
#### ou
```
Mensagem de exceção: 500 
```

## Buscar pedido pelo id.
`GET` (/pedidos/{id}) 
#### Retorno: 
```
200 OK
{
  "dataPedido": "2022-06-23",
  "descricao": "string",
  "idAdm": 0,
  "idUsuario": 0,
  "valorTotal": 0
}
```
#### ou
```
Status: 400
```
ou
```
Mensagem de exceção: 500
```


## Alterar status do pedido para cancelado.
`PUT` (/pedidos/{id}/cancelado)
#### Retorno: 
```
200 OK
{
  "dataPedido": "2022-06-23",
  "descricao": "string",
  "idAdm": 0,
  "idUsuario": 0,
  "valorTotal": 0
}
```
#### ou
```
Status: 400
```
#### ou
```
Mensagem de exceção: 500 
```

## Alterar status do pedido para confirmado.
`PUT` (/pedidos/{id}/confirmado)
#### Retorno: 
```
200 OK
{
  "dataPedido": "2022-06-23",
  "descricao": "string",
  "idAdm": 0,
  "idUsuario": 0,
  "valorTotal": 0
}
```
#### ou
```
Status: 400
```
#### ou
```
Mensagem de exceção: 500
```

## Buscar pedido pelo status.
#### `GET` (/pedidos/{id})
#### Retorno: 
```
200 Ok

{
  "content": [
    {
      "dataPedido": "2022-06-23",
      "descricao": "string",
      "id": 0,
      "idAdm": 0,
      "idUsuario": 0,
      "status": "string",
      "valorTotal": 0
    }
  ]
}
```
#### ou
```
Status: 400
```
#### ou
```
Mensagem de exceção: 500 
```

## Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git][git], [Java 17][Java17] com [Spring Boot][SpringBoot], [MySQL][MySQL] configurado.

## Editar a aplicação ou rodar localmente

```bash

# Clone este repositório em sua máquina  
$ git clone git@gitlab.com:dg2grupo2/back-end/api-pedidos.git

```
---

## 🛠 Tecnologias

As seguintes linguagens/tecnologias foram usadas na construção do projeto:

- [Java 11][Java11]
- [Spring Boot][SpringBoot]
- [Kubernetes (K8s)][k8s]
- [Docker][docker]
- [Git][git]
- [AWS][aws]
   - [SQS][sqs]
   - [SES][ses]
   - [New Relic][NewRelic]

---

## 🛠 Ferramentas

- [JIRA][jira]
- [Draw.io][draw.io]

---

## 🦸 Time de desenvolvimento

⚙️**Carolina Marques** - [Gitlab](https://gitlab.com/carumarques) [Linkedin](https://www.linkedin.com/in/caru-marques/)
⚙️**Silvano Araújo** - [Gitlab](https://gitlab.com/silvanoeng) [Linkedin](https://www.linkedin.com/in/silvano-araujo/)
⚙️**Jean Pierre** - [Gitlab](https://gitlab.com/JeanSisse) [Linkedin](https://www.linkedin.com/in/jeanpierresisse/)
⚙️**Eduardo Gomes** - [Gitlab](https://gitlab.com/eduardo377) [Linkedin](https://www.linkedin.com/in/eduardogomes377/)
⚙️**Ana Beatriz Nunes** - [Gitlab](https://gitlab.com/ananuness) [Linkedin](https://www.linkedin.com/in/ana-beatriz-nunes/)

---

[MySQL]:  https://www.mysql.com/
[SpringBoot]: https://spring.io/projects/spring-boot
[Java11]: https://www.oracle.com/br/java/technologies/javase/jdk11-archive-downloads.html
[jira]: https://dg2grupo2.atlassian.net/jira/software/projects/DG2GRUP/boards/1
[aws]:https://aws.amazon.com/pt/?nc2=h_lg
[k8s]:https://kubernetes.io/pt-br/
[docker]:https://docs.docker.com/
[git]:https://git-scm.com
[sqs]:https://aws.amazon.com/pt/sqs/
[ses]:https://aws.amazon.com/pt/ses/
[NewRelic]:https://aws.amazon.com/pt/quickstart/architecture/new-relic-infrastructure/
[draw.io]:https://app.diagrams.net/
[ApiAdministradores]: https://gitlab.com/dg2grupo2/back-end/api-administradores