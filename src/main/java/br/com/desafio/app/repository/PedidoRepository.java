package br.com.desafio.app.repository;

import br.com.desafio.app.entity.Pedido;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {
    @Query(value = "SELECT * FROM pedidos", nativeQuery = true)
    Page<Pedido> findAllPedidos(Pageable pageable);

    @Query(value = "SELECT * FROM pedidos p " +
            "WHERE p.status= :status", nativeQuery = true)
    Page<Pedido> findAllPedidosPorStatus(Pageable pageable, String status);


}
