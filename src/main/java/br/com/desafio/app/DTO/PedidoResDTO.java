package br.com.desafio.app.DTO;

import br.com.desafio.app.entity.Pedido;
import java.time.LocalDate;

public class PedidoResDTO {
    private Long id;
    private Long idAdm;
    private Long idUsuario;
    private Double valorTotal;
    private String descricao;
    private LocalDate dataPedido;
    private String status;

    public PedidoResDTO() {
    }

    public PedidoResDTO(Long id, Long idAdm, Long idUsuario, Double valorTotal, String descricao, LocalDate dataPedido) {
        this.id = id;
        this.idAdm = idAdm;
        this.idUsuario = idUsuario;
        this.valorTotal = valorTotal;
        this.descricao = descricao;
        this.dataPedido = dataPedido;
    }

    public PedidoResDTO(Pedido pedido) {
        this.id = pedido.getId();
        this.idAdm = pedido.getIdAdm();
        this.idUsuario = pedido.getIdUsuario();
        this.valorTotal = pedido.getValorTotal();
        this.descricao = pedido.getDescricao();
        this.dataPedido = pedido.getDataPedido();
        this.status = pedido.getStatus();
    }

    public String getStatus() {
        return status;
    }

    public Long getId() {
        return id;
    }

    public Long getIdAdm() {
        return idAdm;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public String getDescricao() {
        return descricao;
    }

    public LocalDate getDataPedido() {
        return dataPedido;
    }
}
