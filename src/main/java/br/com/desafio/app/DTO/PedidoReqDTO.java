package br.com.desafio.app.DTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;

public class PedidoReqDTO {

    @NotNull(message = "Não foi inserido o id do administrador.")
    @PositiveOrZero(message = "Valores negativos, não são validos para id do administrador.")
    private Long idAdm;

    @NotNull(message = "Não foi inserido o id do usuario.")
    @PositiveOrZero(message = "Valores negativos, não são validos para id do usuario.")
    private Long idUsuario;

    @NotNull(message = "Deve ser inserido o valor do pedido.")
    @PositiveOrZero(message = "Valores negativos, não são validos para o valor do pedido.")
    private Double valorTotal;

    @NotBlank(message = "Deve ser inserido uma descrição ao pedido.")
    private String descricao;

    @PastOrPresent(message = "Datas futuras não são aceitas.")
    private LocalDate dataPedido;

    private String email;

    public PedidoReqDTO() {
    }

    public PedidoReqDTO(Long idAdm, Long idUsuario, Double valorTotal, String descricao, LocalDate dataPedido, String email) {
        this.idAdm = idAdm;
        this.idUsuario = idUsuario;
        this.valorTotal = valorTotal;
        this.descricao = descricao;
        this.dataPedido = dataPedido;
        this.email = email;
    }

    public PedidoReqDTO(Long idAdm, Long idUsuario, Double valorTotal, String descricao) {
        this.idAdm = idAdm;
        this.idUsuario = idUsuario;
        this.valorTotal = valorTotal;
        this.descricao = descricao;
    }

    public Long getIdAdm() {
        return idAdm;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public String getDescricao() {
        return descricao;
    }

    public LocalDate getDataPedido() {
        return dataPedido;
    }

    public String getEmail() {
        return email;
    }
}
