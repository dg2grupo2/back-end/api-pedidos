package br.com.desafio.app.controller;

import br.com.desafio.app.DTO.PedidoReqDTO;
import br.com.desafio.app.DTO.PedidoResDTO;
import br.com.desafio.app.service.PedidoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
@CrossOrigin("*")
@RestController
@RequestMapping("/pedidos")
@Api(value = "Pedidos")
public class PedidoController {
    private PedidoService<PedidoReqDTO, PedidoResDTO> pedidoService;

    @Autowired
    public PedidoController(PedidoService<PedidoReqDTO, PedidoResDTO> pedidoService) {
        this.pedidoService = pedidoService;
    }

    @ApiOperation(value = "Realiza Cadastro de Pedidos.")
    @PostMapping
    public ResponseEntity<PedidoResDTO> cadastrarPedido(@RequestBody @Valid PedidoReqDTO pedido) {
        try {
            PedidoResDTO pedidoDTO = pedidoService.cadastrarPedido(pedido);
            if (pedidoDTO != null){
                return ResponseEntity.status(201).body(pedidoDTO);
            }
            return ResponseEntity.status(400).body(null);
        }
        catch (Exception e){
            return ResponseEntity.status(500).body(null);
        }
    }

    @ApiOperation(value = "Retorna Lista de Pedidos.")
    @GetMapping
    public ResponseEntity<Page<PedidoResDTO>> listarPedidos(Pageable pageable){
        try {
            Page<PedidoResDTO> pedidoDTOList = pedidoService.listarPedidos(pageable);
            if (pedidoDTOList != null) {
                return ResponseEntity.status(200).body(pedidoDTOList);
            }
            return ResponseEntity.status(404).body(null);
        }
        catch (Exception e){
            return ResponseEntity.status(500).body(null);
        }
    }

    @ApiOperation(value = "Realiza Busca de Pedidos pelo Status.")
    @GetMapping("/busca")
    public ResponseEntity<Page<PedidoResDTO>> listarPedidosPorStatus(Pageable pageable, @RequestParam String status){
        try {
            Page<PedidoResDTO> pedidoDTOList = pedidoService.listarPedidosPorStatus(pageable, status);
            if (pedidoDTOList != null) {
                return ResponseEntity.status(200).body(pedidoDTOList);
            }
            return ResponseEntity.status(404).body(null);
        }
        catch (Exception e){
            return ResponseEntity.status(500).body(null);
        }
    }

    @ApiOperation(value = "Realiza Busca de Pedido pelo id.")
    @GetMapping("/{id}")
    public ResponseEntity<PedidoResDTO> buscarPedido(@PathVariable Long id){
        PedidoResDTO pedido = pedidoService.buscarPedido(id);
        return ResponseEntity.status(200).body(pedido);
    }
    @ApiOperation(value = "Realiza Alteração do Status do Pedido para Confirmado.")
    @PutMapping("/{id}/confirmado")
    public ResponseEntity<PedidoResDTO> statusConfirmado(@PathVariable Long id) {
        PedidoResDTO pedido = pedidoService.statusConfirmado(id);
        return ResponseEntity.status(201).body(pedido);
    }

    @ApiOperation(value = "Realiza Alteração do Status do Pedido para Cancelado.")
    @PutMapping("/{id}/cancelado")
    public ResponseEntity<PedidoResDTO> statusCancelado(@PathVariable Long id) {
        PedidoResDTO pedido = pedidoService.statusCancelado(id);
        return ResponseEntity.status(201).body(pedido);
    }

}
