package br.com.desafio.app.service.impl;

import br.com.desafio.app.DTO.PedidoResDTO;
import br.com.desafio.app.aws.ses.EmailService;
import br.com.desafio.app.controller.exceptions.EntityNotFoundException;
import br.com.desafio.app.entity.Pedido;
import br.com.desafio.app.DTO.PedidoReqDTO;
import br.com.desafio.app.service.PedidoService;
import br.com.desafio.app.repository.PedidoRepository;
import br.com.desafio.app.aws.sqs.ProducerSQS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;


@Service
public class PedidoServiceImpl implements PedidoService<PedidoReqDTO, PedidoResDTO> {
    private static final Logger LOG = LoggerFactory.getLogger(PedidoServiceImpl.class);
    private PedidoRepository pedidoRepository;
    private ProducerSQS producerSQS;
    private EmailService emailService;

    @Autowired
    public PedidoServiceImpl(PedidoRepository pedidoRepository, ProducerSQS producerSQS, EmailService emailService) {
        this.pedidoRepository = pedidoRepository;
        this.producerSQS = producerSQS;
        this.emailService = emailService;
    }

    @Override
    public PedidoResDTO cadastrarPedido(PedidoReqDTO pedidoDTO) {

        Pedido pedido = new Pedido(pedidoDTO);
        if (pedido.getDataPedido() == null) {
            ZoneId zone = ZoneId.systemDefault();
            LocalDate now = LocalDate.ofInstant(Instant.now(), zone);
            pedido.setDataPedido(now);
        }
        pedido.setStatus("processando");
        PedidoResDTO pedidoResDTO = new PedidoResDTO(pedidoRepository.saveAndFlush(pedido));
        LOG.info("O pedido de id {} foi salvo com sucesso.", pedidoResDTO.getId());
        producerSQS.mandarMensagem(pedidoResDTO, pedidoDTO.getEmail());
        return pedidoResDTO;
    }

    @Override
    public Page<PedidoResDTO> listarPedidos(Pageable pageable) {
        Page<Pedido> pedidos = pedidoRepository.findAllPedidos(pageable);
        Page<PedidoResDTO> pedidosDTO = pedidos.map(x -> new PedidoResDTO(x));
        return pedidosDTO;
    }

    @Override
    public Page<PedidoResDTO> listarPedidosPorStatus(Pageable pageable, String status) {
        Page<Pedido> pedidos = pedidoRepository.findAllPedidosPorStatus(pageable, status);
        Page<PedidoResDTO> pedidosDTO = pedidos.map(x -> new PedidoResDTO(x));
        return pedidosDTO;
    }

    @Override
    public PedidoResDTO buscarPedido(Long id) {
        return new PedidoResDTO(pedidoRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Id " + id + " não encontrado.")
        ));
    }

    @Override
    public PedidoResDTO statusConfirmado(Long id) {
        Pedido pedidoBanco = pedidoRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Id " + id + " não encontrado.")
        );
        pedidoBanco.setStatus("confirmado");
        PedidoResDTO pedidoResDTO = new PedidoResDTO(pedidoRepository.save(pedidoBanco));
        LOG.info("O pedido de id {} foi salvo com sucesso.", pedidoResDTO.getId());
        return pedidoResDTO;
    }

    @Override
    public PedidoResDTO statusConfirmadoEmail(Long id, String email) {
        Pedido pedidoBanco = pedidoRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Id " + id + " não encontrado.")
        );
        pedidoBanco.setStatus("confirmado");
        PedidoResDTO pedidoResDTO = new PedidoResDTO(pedidoRepository.save(pedidoBanco));
        LOG.info("O pedido de id {} foi salvo com sucesso.", pedidoResDTO.getId());
        emailService.enviarMensagem(pedidoResDTO, email);
        return pedidoResDTO;
    }


    @Override
    public PedidoResDTO statusCancelado(Long id) {
        Pedido pedidoBanco = pedidoRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Id " + id + " não encontrado.")
        );
        pedidoBanco.setStatus("cancelado");
        pedidoBanco = pedidoRepository.save(pedidoBanco);
        return new PedidoResDTO(pedidoBanco);
    }

}
