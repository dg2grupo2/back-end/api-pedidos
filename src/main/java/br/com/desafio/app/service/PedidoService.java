package br.com.desafio.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PedidoService<T,C> {
    C cadastrarPedido(T t);
    Page<C> listarPedidos(Pageable pageable);
    Page<C> listarPedidosPorStatus(Pageable pageable, String status);
    C buscarPedido(Long id);
    C statusConfirmado(Long id);
    C statusConfirmadoEmail(Long id, String email);
    C statusCancelado(Long id);
}
