package br.com.desafio.app.security.filters;

import br.com.desafio.app.security.AdministradorClient;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SystemAuthFilter extends OncePerRequestFilter {
    AdministradorClient administradorClient = new AdministradorClient();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if ( request.getServletPath().contains("/swagger") || request.getServletPath().contains("/webjar") ||
                request.getServletPath().contains("/v2/api-docs") ) {
            SecurityContextHolder.getContext().setAuthentication(administradorClient.isAuthenticated());
            System.out.println(request.getServletPath());
            filterChain.doFilter(request, response);
            return;
        }

        Authentication auth = administradorClient.isAuthorized(request.getHeader("Authorization"));
        SecurityContextHolder.getContext().setAuthentication(auth);
        filterChain.doFilter(request, response);
    }
}
