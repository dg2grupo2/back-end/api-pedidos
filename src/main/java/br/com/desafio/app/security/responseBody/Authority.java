package br.com.desafio.app.security.responseBody;

import org.springframework.security.core.GrantedAuthority;

public class Authority implements GrantedAuthority {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String authority;

    @Override
    public String getAuthority() {
        return this.name;
    }
}
