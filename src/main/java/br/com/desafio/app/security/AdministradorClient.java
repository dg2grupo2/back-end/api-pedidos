package br.com.desafio.app.security;

import br.com.desafio.app.security.responseBody.ResponseBody;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.io.IOException;


public class AdministradorClient {

    private String urlApiAdm="admin-container";
    //private String urlApiAdm="localhost";

    HttpClient client = HttpClientBuilder.create().build();

    public UsernamePasswordAuthenticationToken isAuthenticated() {
        ResponseBody responseBody = new ResponseBody();
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(responseBody.getPrincipal(), responseBody.getCredentials(), responseBody.getAuthorities());
        return usernamePasswordAuthenticationToken;
    }

    public UsernamePasswordAuthenticationToken isAuthorized(String token) {
        if (token == null) {
            return new UsernamePasswordAuthenticationToken(null, null);
        }
        try {

            HttpPost post = new HttpPost("http://" + urlApiAdm + ":8089/validate");
            post.addHeader("Content-Type", "Application/json");
            post.addHeader("Authorization", token);

            HttpResponse res = client.execute(post);

            String body = EntityUtils.toString(res.getEntity());

            ResponseBody responseBody = new Gson().fromJson(body, ResponseBody.class);
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                    responseBody.getPrincipal(), responseBody.getCredentials(), responseBody.getAuthorities());
            return usernamePasswordAuthenticationToken;
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
