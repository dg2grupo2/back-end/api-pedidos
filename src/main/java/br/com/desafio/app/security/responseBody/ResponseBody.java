package br.com.desafio.app.security.responseBody;

import java.util.Collection;

public class ResponseBody {
    private Object Principal;
    private Object Credentials;
    private Collection<Authority> authorities;
    private Boolean authenticated;

    public Object getPrincipal() {
        return Principal;
    }

    public void setPrincipal(Object principal) {
        Principal = principal;
    }

    public Object getCredentials() {
        return Credentials;
    }

    public void setCredentials(Object credentials) {
        Credentials = credentials;
    }

    public Collection<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<Authority> authorities) {
        this.authorities = authorities;
    }

    public Boolean getAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(Boolean authenticated) {
        this.authenticated = authenticated;
    }
}
