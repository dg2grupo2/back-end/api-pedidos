package br.com.desafio.app.security.filters;

import br.com.desafio.app.security.filters.exceptions.SystemEntryPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SystemSecurityConfiguration {

    SystemEntryPoint entryPoint;

    public SystemSecurityConfiguration(SystemEntryPoint entryPoint) {
        this.entryPoint = entryPoint;
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http.csrf().disable()
                .cors().and()
                .exceptionHandling().authenticationEntryPoint(entryPoint)
                .and()
                .authorizeRequests(auth -> auth.antMatchers( "/swagger*",
                                                                            "/v2/api-docs",
                                                                            "/swagger-resources/**",
                                                                            "/swagger-ui.html",
                                                                            "/webjars/**",
                                                                            "/swagger.json").permitAll())

                .authorizeRequests(auth -> auth.anyRequest().authenticated())
                .addFilterBefore(new SystemAuthFilter(), UsernamePasswordAuthenticationFilter.class)
                .build();
    }
}
