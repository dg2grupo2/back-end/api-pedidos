package br.com.desafio.app.security.filters.exceptions;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SystemEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException, ServletException {
        String json = String.format("{\"message\": \"%s\"}", authException.getMessage());
        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        response.setContentType("spplication/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
}
