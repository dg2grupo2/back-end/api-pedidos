package br.com.desafio.app.entity;

import br.com.desafio.app.DTO.PedidoReqDTO;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name= "pedidos")
public class Pedido {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "id_adm")
    private Long idAdm;
    @Column(name = "id_usuario")
    private Long idUsuario;
    @Column(name = "valor_total")
    private Double valorTotal;
    private String descricao;
    @Column(name = "data_pedido")
    private LocalDate dataPedido;
    private String status;

    public Pedido() {
    }

    public Pedido(Long idAdm, Long idUsuario, Double valorTotal, String descricao, LocalDate dataPedido, String status) {
        this.idAdm = idAdm;
        this.idUsuario = idUsuario;
        this.valorTotal = valorTotal;
        this.descricao = descricao;
        this.dataPedido = dataPedido;
        this.status = status;
    }

    public Pedido(PedidoReqDTO pedido) {
        this.idAdm = pedido.getIdAdm();
        this.idUsuario = pedido.getIdUsuario();
        this.valorTotal = pedido.getValorTotal();
        this.descricao = pedido.getDescricao();
        this.dataPedido = pedido.getDataPedido();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAdm() {
        return idAdm;
    }

    public void setIdAdm(Long idAdm) {
        this.idAdm = idAdm;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(LocalDate dataPedido) {
        this.dataPedido = dataPedido;
    }

    public String getStatus() {
        return status;
    }

    public boolean setStatus(String status) {
        if (status.equals("processando") || status.equals("confirmado") || status.equals("cancelado") ){
            this.status = status;
            return true;
        }
        return false;
    }
}
