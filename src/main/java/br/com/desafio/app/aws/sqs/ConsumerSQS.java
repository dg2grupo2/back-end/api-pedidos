package br.com.desafio.app.aws.sqs;

import br.com.desafio.app.service.impl.PedidoServiceImpl;
import br.com.desafio.app.aws.entity.Header;
import br.com.desafio.app.aws.entity.Notification;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.awspring.cloud.messaging.listener.annotation.SqsListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ConsumerSQS {
    private static final Logger LOG = LoggerFactory.getLogger(ConsumerSQS.class);

    private PedidoServiceImpl pedidoService;

    @Autowired
    public ConsumerSQS(PedidoServiceImpl pedidoService) {
        this.pedidoService = pedidoService;
    }

    @SqsListener(value = "G2_D2_SQS")
    public void receberMensagem(String mensagem){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Notification notification = objectMapper.readValue(mensagem, Notification.class);
            Header headerIdPedido = notification.getAtributosMensagem().get("idPedido");
            Header headerValorPedido = notification.getAtributosMensagem().get("valorPedido");
            Header headerEmailUsuario = notification.getAtributosMensagem().get("emailUsuario");
            Long idPedido = Long.parseLong(headerIdPedido.getValue());
            Double valorPedido = Double.parseDouble(headerValorPedido.getValue());
            pedidoService.statusConfirmadoEmail(idPedido, headerEmailUsuario.getValue());
            LOG.info("O pedido id {}, com o valor de R${} teve seu status alterado para confirmado.", idPedido, valorPedido);
        }catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }
}
