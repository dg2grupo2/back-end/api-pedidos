package br.com.desafio.app.aws.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Notification {

    @JsonProperty("Message")
    private String mensagem;

    @JsonProperty("MessageAttributes")
    private Map<String, Header> atributosMensagem;

    public Notification() {
    }

    public Notification(String mensagem, Map<String, Header> atributosMensagem) {
        this.mensagem = mensagem;
        this.atributosMensagem = atributosMensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    public Map<String, Header> getAtributosMensagem() {
        return atributosMensagem;
    }

}
