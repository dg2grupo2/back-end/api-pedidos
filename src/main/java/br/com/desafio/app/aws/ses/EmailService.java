package br.com.desafio.app.aws.ses;

import br.com.desafio.app.DTO.PedidoResDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.mail.MailSender;

@Service
public class EmailService {
    private static final Logger LOG = LoggerFactory.getLogger(EmailService.class);
    private MailSender mailSender;

    @Autowired
    public EmailService(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void enviarMensagem(PedidoResDTO pedido, String emailUsuario){
        try {
            SimpleMailMessage email = new SimpleMailMessage();

            email.setFrom("confirma.compra.g2.d2@gmail.com");
            email.setTo(emailUsuario);
            email.setSubject("Confirmação do pedido realizado na G2.D2");
            email.setText("Seu pedido id " + pedido.getId() + " no valor de R$ " + pedido.getValorTotal() +
                    " foi comfirmado e o status do mesmo foi alterado para confirmado. \n A G2.D2 agradece a preferência.");
            this.mailSender.send(email);
            LOG.info("Email confirmando o pedido id {}.", pedido.getId());
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

    }
}
