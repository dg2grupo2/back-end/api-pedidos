package br.com.desafio.app.aws.sqs;

import br.com.desafio.app.DTO.PedidoResDTO;
import br.com.desafio.app.aws.entity.Header;
import br.com.desafio.app.aws.entity.Notification;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.awspring.cloud.messaging.core.QueueMessagingTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ProducerSQS {
    private static final Logger LOG = LoggerFactory.getLogger(ProducerSQS.class);
    @Value("${cloud.aws.end-point.uri}")
    private String destino;
    private QueueMessagingTemplate queueMessagingTemplate;

    @Autowired
    public ProducerSQS(QueueMessagingTemplate queueMessagingTemplate) {
        this.queueMessagingTemplate = queueMessagingTemplate;
    }

    public void mandarMensagem(PedidoResDTO pedido, String email){
        try {
            String mensagem = "O pedido id "+ pedido.getId() + " esta com status processando.";
            Map<String, Header> headerMap = new HashMap<String, Header>();
            headerMap.put("idPedido", new Header("Long",String.valueOf(pedido.getId())));
            headerMap.put("valorPedido", new Header("Double", String.valueOf(pedido.getValorTotal())));
            headerMap.put("emailUsuario", new Header("String", email));
            Notification mensagemSQS = new Notification(mensagem, headerMap);
            ObjectMapper objectMapper = new ObjectMapper();
            Message novaMensagem = MessageBuilder.withPayload(objectMapper.writeValueAsString(mensagemSQS))
                    .build();
            queueMessagingTemplate.send(destino, novaMensagem );
            LOG.info("Mensagem enviada. {}", mensagem);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

    }
}
