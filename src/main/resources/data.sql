drop table if exists pedidos;

CREATE TABLE pedidos (
  id BIGINT NOT NULL AUTO_INCREMENT,
  id_adm BIGINT NOT NULL,
  id_usuario BIGINT NOT NULL,
  valor_total DECIMAL(10,2) NOT NULL,
  descricao VARCHAR(255) DEFAULT NULL,
  data_pedido DATE DEFAULT NULL,
  status VARCHAR(40) DEFAULT NULL,
  PRIMARY KEY (id));

INSERT INTO pedidos ( id_adm, id_usuario, valor_total, descricao, data_pedido, status)
VALUES ( 1, 1, 124.40, 'Mouse gamer, cor preto e vermelho', curdate(), 'confirmado'),
( 1, 2, 4.25, 'Porta caneta, cor vermelho', curdate(), 'cancelado'),
( 3, 3, 1524.11, 'Notebook, cor preto', curdate(), 'confirmado'),
( 2, 4, 12.00, 'Fone, cor prata', curdate(), 'confirmado');