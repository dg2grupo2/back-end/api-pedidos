package br.com.desafio.app.service.impl;

import br.com.desafio.app.DTO.PedidoReqDTO;
import br.com.desafio.app.DTO.PedidoResDTO;
import br.com.desafio.app.aws.ses.EmailService;
import br.com.desafio.app.aws.sqs.ProducerSQS;
import br.com.desafio.app.entity.Pedido;
import br.com.desafio.app.repository.PedidoRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class PedidosServicesTest {
    @InjectMocks
    private PedidoServiceImpl instance;

    @Mock
    private PedidoRepository pedidoRepository;

    @Mock
    private ProducerSQS producerSQS;

    @Mock
    private EmailService emailService;

    @Captor
    ArgumentCaptor<Pedido> pedidoCaptor;

    @Test
    @DisplayName("Deve cadastrar um pedido e retornar o mesmo, com status de processando," +
            " id igual a 2 e com a data instanciada neste momento.")
    void cadastrandoPedidoSemData(){
        //given:
        PedidoReqDTO pedidoReqDTO = pedidoReqFake("SemData");
        when(pedidoRepository.saveAndFlush(pedidoCaptor.capture())).thenAnswer(invocationOnMock -> {
            Pedido value = pedidoCaptor.getValue();
            value.setId(2L);
            return value;
        });

        ZoneId zone = ZoneId.systemDefault();
        LocalDate now = LocalDate.ofInstant(Instant.now(), zone);

        //when:
        PedidoResDTO pedidoResDTO = instance.cadastrarPedido(pedidoReqDTO);

        //Then:
        assertThat(pedidoResDTO).isNotNull();
        assertThat(pedidoResDTO.getStatus()).isEqualTo("processando");
        assertThat(pedidoResDTO.getId()).isEqualTo(2);
        assertThat(pedidoResDTO.getDataPedido()).isEqualTo(now);
    }
    @Test
    @DisplayName("Deve cadastrar um pedido com data e retornar o mesmo, com a data anterior a hoje.")
    void cadastrandoPedidoComData(){
        //given:
        PedidoReqDTO pedidoReqDTO = pedidoReqFake("ComData");
        when(pedidoRepository.saveAndFlush(pedidoCaptor.capture())).thenAnswer(invocationOnMock -> {
            Pedido value = pedidoCaptor.getValue();
            value.setId(2L);
            return value;
        });

        ZoneId zone = ZoneId.systemDefault();
        LocalDate now = LocalDate.ofInstant(Instant.now(), zone);

        //when:
        PedidoResDTO pedidoResDTO = instance.cadastrarPedido(pedidoReqDTO);

        //Then:
        assertThat(pedidoResDTO).isNotNull();
        assertThat(pedidoResDTO.getDataPedido()).isBefore(now);
    }

    @Test
    @DisplayName("Mudar o status do pedido para confirmado")
    void statusConfirmado(){
        //given:
        Pedido pedido = pedidoFake();
        pedido.setId(3L);
        Long id = 3L;
        Optional<Pedido> optionalPedido = Optional.of(pedido);

        when(pedidoRepository.findById(anyLong())).thenReturn(optionalPedido);

        when(pedidoRepository.save(pedidoCaptor.capture())).thenAnswer(invocationOnMock -> {
            Pedido value = pedidoCaptor.getValue();
            return value;
        });

        //when:
        PedidoResDTO pedidoResDTO = instance.statusConfirmado(id);

        //Then:
        assertThat(pedidoResDTO).isNotNull();
        assertThat(pedidoResDTO.getStatus()).isEqualTo("confirmado");
    }

    @Test
    @DisplayName("Mudar o status do pedido para cancelado")
    void statusCancelado(){
        //given:
        Pedido pedido = pedidoFake();
        pedido.setId(3L);
        pedido.setStatus("confirmado");
        Long id = 3L;
        Optional<Pedido> optionalPedido = Optional.of(pedido);

        when(pedidoRepository.findById(anyLong())).thenReturn(optionalPedido);

        when(pedidoRepository.save(pedidoCaptor.capture())).thenAnswer(invocationOnMock -> {
            Pedido value = pedidoCaptor.getValue();
            return value;
        });

        //when:
        PedidoResDTO pedidoResDTO = instance.statusCancelado(id);

        //Then:
        assertThat(pedidoResDTO).isNotNull();
        assertThat(pedidoResDTO.getStatus()).isEqualTo("cancelado");
    }

    private PedidoReqDTO pedidoReqFake(String date) {
        if (date.equalsIgnoreCase("SemData")) {
            PedidoReqDTO pedidoReqDTO = new PedidoReqDTO(2L, 4L, 124.40,
                    "Mouse gamer, cor preto e vermelho");
            return pedidoReqDTO;
        }
        LocalDate data = LocalDate.of(2022,06,13);
        if (date.equalsIgnoreCase("ComData")) {
            PedidoReqDTO pedidoReqDTO = new PedidoReqDTO(2L, 4L, 124.40,
                    "Mouse gamer, cor preto e vermelho", data, "silvano@homail.com");
            return pedidoReqDTO;
        }
        return null;
    }

    private Pedido pedidoFake() {
        ZoneId zone = ZoneId.systemDefault();
        LocalDate now = LocalDate.ofInstant(Instant.now(), zone);
        Pedido pedido = new Pedido(2L, 4L, 124.40,
                "Mouse gamer, cor preto e vermelho", now, "processando");
        return pedido;
    }


}
