FROM openjdk:11
LABEL maintainer="Grupo 2 - desafio final <grupo02.ilab@gmail.com>"

ARG JAR_FILE=target/*.jar
ARG NEW_RELIC_KEY

COPY ${JAR_FILE} app.jar

RUN mkdir -p /newrelic
ADD ./newrelic/newrelic.jar /newrelic/newrelic.jar
ADD ./newrelic/newrelic.yml /newrelic/newrelic.yml

ENV NEW_RELIC_APP_NAME="API_PEDIDOS"
ENV NEW_RELIC_LICENSE_KEY=$NEW_RELIC_KEY
ENV NEW_RELIC_LOG_FILE_NAME=STDOUT

EXPOSE 8083

ENTRYPOINT ["java","-javaagent:/newrelic/newrelic.jar","-jar","app.jar"]
